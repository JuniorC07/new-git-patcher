FROM ubuntu:20.04

COPY --from=mikefarah/yq:4 /usr/bin/yq /usr/bin/yq

RUN apt-get update && apt-get install -yy git curl && rm -rf /var/lib/apt/lists/*

RUN git config --global user.name "Devops" && \
    git config --global user.email "junior.camargo@unochapeco.edu.br"

COPY ./scripts /usr/local/bin/

RUN chmod +x /usr/local/bin/*
